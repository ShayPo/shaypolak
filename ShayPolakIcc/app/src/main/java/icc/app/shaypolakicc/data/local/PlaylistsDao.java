package icc.app.shaypolakicc.data.local;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;

import java.util.ArrayList;
import java.util.List;

import icc.app.shaypolakicc.data.model.Playlists;

public class PlaylistsDao
{
    private List<Playlists> mData;
    private MutableLiveData<List<Playlists>> mPlaylists;

    PlaylistsDao()
    {
        mData = new ArrayList<>();
        mPlaylists = new MutableLiveData<>();
        mPlaylists.setValue(mData);
    }

    public LiveData<List<Playlists>> getPlaylists() {
        return mPlaylists;
    }
}