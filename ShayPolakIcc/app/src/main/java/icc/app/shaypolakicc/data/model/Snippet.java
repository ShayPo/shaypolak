package icc.app.shaypolakicc.data.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Snippet
{
    @SerializedName("thumbnails")
    @Expose
    private Thumbnails mThumbnails;
    @SerializedName("title")
    @Expose
    private String mTitle;


    public String getTitle() {
        return mTitle;
    }

    public void setTitle(String title)
    {
        this.mTitle = title;
    }

    public Thumbnails getThumbnails() {
        return mThumbnails;
    }

    public void setThumbnails(Thumbnails thumbnails) {
        this.mThumbnails = thumbnails;
    }
}
