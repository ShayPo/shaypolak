package icc.app.shaypolakicc.ui.playlists;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.ViewModel;
import icc.app.shaypolakicc.data.VideoListRepository;
import icc.app.shaypolakicc.data.model.VideoList;

public class VideoListViewModel extends
        ViewModel {
    private VideoListRepository mRepository;

    public VideoListViewModel(VideoListRepository repository)
    {
        mRepository = repository;
    }

    public LiveData<VideoList> getVideoList()
    {
        return mRepository.getVideoList();
    }
}
