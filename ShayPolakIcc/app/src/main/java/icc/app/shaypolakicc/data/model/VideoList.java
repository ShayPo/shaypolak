package icc.app.shaypolakicc.data.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class VideoList
{
    @SerializedName("playlistItems")
    @Expose
    private Playlist mPlaylist;
    @SerializedName("snippet")
    @Expose
    private Snippet mSnippet;

    public Snippet getSnippet() {
        return mSnippet;
    }

    public void setSnippet(Snippet snippet) {
        this.mSnippet = mSnippet;
    }

    public void setPlaylistItems(Playlist playlistItems) {
        mPlaylist = playlistItems;
    }

    public Playlist getPlaylistItems() {
        return mPlaylist;
    }
}