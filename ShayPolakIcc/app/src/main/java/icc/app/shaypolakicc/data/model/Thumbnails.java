package icc.app.shaypolakicc.data.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Thumbnails
{
    @SerializedName("default")
    @Expose
    private ImageUrl mImageUrl;

    public ImageUrl getImageUrl() {
        return mImageUrl;
    }

    public void setImageUrl(ImageUrl imageUrl) {
        this.mImageUrl = imageUrl;
    }
}
