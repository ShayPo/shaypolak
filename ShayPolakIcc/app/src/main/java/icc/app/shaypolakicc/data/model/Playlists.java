package icc.app.shaypolakicc.data.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Playlists {
    @SerializedName("items")
    @Expose
    private List<VideoList> items;

    public void setItems(List<VideoList> items) {
        this.items = items;
    }

    public List<VideoList> getItems() {
        return items;
    }
}
