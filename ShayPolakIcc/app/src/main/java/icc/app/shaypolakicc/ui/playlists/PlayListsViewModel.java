package icc.app.shaypolakicc.ui.playlists;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.ViewModel;

import icc.app.shaypolakicc.data.PlayListsRepository;
import icc.app.shaypolakicc.data.model.Playlists;

public class PlayListsViewModel extends
        ViewModel {
    private PlayListsRepository mRepository;

    PlayListsViewModel(PlayListsRepository repository)
    {
        mRepository = repository;
    }

    public LiveData<Playlists> getPlayLists()
    {
        return mRepository.getPlayLists();
    }
}
