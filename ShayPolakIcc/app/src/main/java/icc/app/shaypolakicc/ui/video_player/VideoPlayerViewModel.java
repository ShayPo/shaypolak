package icc.app.shaypolakicc.ui.video_player;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.ViewModel;

import icc.app.shaypolakicc.data.VideoListRepository;
import icc.app.shaypolakicc.data.VideoRepository;
import icc.app.shaypolakicc.data.model.Video;
import icc.app.shaypolakicc.data.model.VideoList;

public class VideoPlayerViewModel extends ViewModel {
    private VideoRepository mRepository;

    VideoPlayerViewModel(VideoRepository repository) {
        mRepository = repository;
    }

    public LiveData<Video> getVideo() {
        return mRepository.getVideo();
    }

    public void updateVideoTime(Float time) {
         getVideo().getValue().setVideoTime(time);
    }
}