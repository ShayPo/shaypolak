package icc.app.shaypolakicc.data.server;

import icc.app.shaypolakicc.data.model.Playlists;
import retrofit2.Call;
import retrofit2.http.GET;

public interface PlaylistsAPI
{
    @GET("playlists.json")
    Call<Playlists> getPlaylists();
}
