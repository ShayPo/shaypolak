package icc.app.shaypolakicc.data.local;

//TODO: delete this class
public class Database
{

    static private Database mInstanse;
    private PlaylistsDao mPlaylistsDao;

    private Database(){}

    public PlaylistsDao getPlaylistsDao() {
        return mPlaylistsDao;
    }

    static synchronized public Database GetInstance()
    {
        if(mInstanse == null)
        {
            mInstanse = new Database();
        }
        return mInstanse;
    }

}
