package icc.app.shaypolakicc.ui.video_player;

import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProvider;
import android.support.annotation.NonNull;

import icc.app.shaypolakicc.data.VideoListRepository;
import icc.app.shaypolakicc.data.VideoRepository;
import icc.app.shaypolakicc.ui.playlists.VideoListViewModel;

public class VideoPlayerViewModelFactory extends ViewModelProvider.NewInstanceFactory
{
    private VideoRepository mRepository;

    public VideoPlayerViewModelFactory(VideoRepository repository)
    {
        mRepository = repository;
    }

    @NonNull
    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        return (T) new VideoPlayerViewModel(mRepository);
    }
}
