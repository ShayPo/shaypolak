package icc.app.shaypolakicc.ui.playlists;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import icc.app.shaypolakicc.R;
import icc.app.shaypolakicc.data.model.Video;

public class PlayListAdapter extends RecyclerView.Adapter<PlayListAdapter.Holder> {

    private List<Video> mData = new ArrayList<>();
    private MutableLiveData<View> mObservable;

    PlayListAdapter() {
        mObservable = new MutableLiveData<>();
        mObservable.setValue(null);
    }

    public LiveData<View> getObservable() {
        return mObservable;
    }

    public void updateData(List<Video> videos) {
        if (videos != null) {
            mData = new ArrayList<>(videos);
            notifyDataSetChanged();
        }
    }

    @NonNull
    @Override
    public Holder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.playlists_list_item, viewGroup, false);
        return new Holder(v, mObservable);
    }

    @Override
    public void onBindViewHolder(@NonNull Holder holder, int i) {
        holder.bind(mData.get(i));
    }

    @Override
    public int getItemCount() {
        return mData == null ? 0 : mData.size();
    }

    public class Holder extends RecyclerView.ViewHolder {

        private ImageView mItemImage;
        private TextView mTitle;

        public Holder(@NonNull View itemView, final MutableLiveData<View> liveData) {
            super(itemView);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    liveData.setValue(v);
                }
            });
            mItemImage = itemView.findViewById(R.id.imageView);
            mTitle = itemView.findViewById(R.id.title);
        }

        public void bind(Video video) {
            itemView.setTag(video);
            mTitle.setText(video.getSnippet().getTitle());
            Picasso.get().load(video.getSnippet().getThumbnails().getImageUrl().getUrl()).into(mItemImage);
        }
    }
}

