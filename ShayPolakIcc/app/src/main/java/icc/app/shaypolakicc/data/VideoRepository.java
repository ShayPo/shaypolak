package icc.app.shaypolakicc.data;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;

import icc.app.shaypolakicc.data.model.Video;

public class VideoRepository
{
    static private VideoRepository mInstanse;
    private MutableLiveData<Video> mVideo;

    private VideoRepository() {
        mVideo = new MutableLiveData<>();
        mVideo.setValue(null);
    }

    public LiveData<Video> getVideo() {
        return mVideo;
    }

    private void setVideo(Video video) {
        mVideo.setValue(video);
    }

    static synchronized public VideoRepository GetInstance(Video video) {
        if (mInstanse == null) {
            mInstanse = new VideoRepository();
        }

        if (video != null) {
            mInstanse.setVideo(video);
        }
        return mInstanse;
    }
}