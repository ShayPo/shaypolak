package icc.app.shaypolakicc.ui.video_player;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.pierfrancescosoffritti.androidyoutubeplayer.player.YouTubePlayer;
import com.pierfrancescosoffritti.androidyoutubeplayer.player.YouTubePlayerView;
import com.pierfrancescosoffritti.androidyoutubeplayer.player.listeners.AbstractYouTubePlayerListener;
import com.pierfrancescosoffritti.androidyoutubeplayer.player.listeners.YouTubePlayerInitListener;

import java.lang.ref.WeakReference;

import icc.app.shaypolakicc.R;
import icc.app.shaypolakicc.data.model.Video;
import icc.app.shaypolakicc.utilities.InjectorUtils;

public class VideoPlayerActivity extends AppCompatActivity {
    private VideoPlayerViewModel mViewModel;
    private Float mTime;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.video_player_activity);
        init();
    }

    private void init() {
        final YouTubePlayerView youTubePlayerView = findViewById(R.id.youtube_player_view);
        mViewModel = ViewModelProviders.of(this, InjectorUtils.provideVideoPlayerViewModelFactory()).get(VideoPlayerViewModel.class);
        mViewModel.getVideo().observe(this, new Observer<Video>() {
            @Override
            public void onChanged(@Nullable Video video)
            {
                final WeakReference<Video> v =  new WeakReference<>(video);
                if (video != null && youTubePlayerView != null) {
                    final String id = video.getId();
                    youTubePlayerView.initialize(
                            new YouTubePlayerInitListener() {
                                @Override
                                public void onInitSuccess(
                                        final YouTubePlayer initializedYouTubePlayer) {

                                    initializedYouTubePlayer.addListener(
                                            new AbstractYouTubePlayerListener() {
                                                @Override
                                                public void onReady() {
                                                    initializedYouTubePlayer.loadVideo(id, v.get().getVideoTime());
                                                }

                                                @Override
                                                public void onCurrentSecond(float second) {
                                                    super.onCurrentSecond(second);
                                                    Log.d("videoplayerTime", "video timeline" + second);
                                                    mTime = second;
                                                }
                                            });
                                }
                            }, true);
                }
            }
        });
    }

    @Override
    protected void onStop() {
        super.onStop();
        mViewModel.updateVideoTime(mTime);
    }
}
