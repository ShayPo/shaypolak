package icc.app.shaypolakicc.ui.playlists;



import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import icc.app.shaypolakicc.R;
import icc.app.shaypolakicc.data.VideoListRepository;
import icc.app.shaypolakicc.data.model.Playlists;
import icc.app.shaypolakicc.data.model.VideoList;
import icc.app.shaypolakicc.utilities.InjectorUtils;

public class PlayListsActivity extends AppCompatActivity {

    private PlayListsViewModel mViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video_lists);
        init();
    }

    private void init()
    {
        final PlayListsAdapter adapter = new PlayListsAdapter();
        RecyclerView list = findViewById(R.id.playlists_display);
        list.setLayoutManager(new GridLayoutManager(this,2, LinearLayoutManager.VERTICAL, false));
        list.setAdapter(adapter);
        adapter.getObservable().observe(this, new Observer<View>() {
            @Override
            public void onChanged(@Nullable View v) {
                if(v != null) {
                    Intent showVideoPlayer = new Intent(PlayListsActivity.this, PlayListActivity.class);
                    VideoListRepository.GetInstance((VideoList) v.getTag());
                    startActivity(showVideoPlayer);
                }
            }
        });

        mViewModel = ViewModelProviders.of(this, InjectorUtils.providePlayListsViewModelFactory()).get(PlayListsViewModel.class);
        mViewModel.getPlayLists().observe(this, new Observer<Playlists>() {
            @Override
            public void onChanged(@Nullable Playlists playlists) {
                adapter.updateData(playlists.getItems());
            }
        });
    }
}
