package icc.app.shaypolakicc.data.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Video
{
    @SerializedName("contentDetails")
    @Expose
    private ContentDetails mContentDetails;
    @SerializedName("snippet")
    @Expose
    private Snippet mSnippet;
    private Float mVideoTime = 0f;

    public void setVideoTime(Float videoTime) {
        this.mVideoTime = videoTime;
    }

    public Float getVideoTime() {
        return mVideoTime;
    }

    public Snippet getSnippet() {
        return mSnippet;
    }

    public void setSnippet(Snippet snippet) {
        this.mSnippet = snippet;
    }

    public ContentDetails getContentDetails() {
        return mContentDetails;
    }

    public void setmContentDetails(ContentDetails contentDetails) {
        this.mContentDetails = contentDetails;
    }

    public String getId() {
        return mContentDetails.getId();
    }

}