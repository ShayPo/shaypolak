package icc.app.shaypolakicc.utilities;

import icc.app.shaypolakicc.data.PlayListsRepository;
import icc.app.shaypolakicc.data.VideoListRepository;
import icc.app.shaypolakicc.data.VideoRepository;
import icc.app.shaypolakicc.ui.playlists.PlayListsViewModelFactory;
import icc.app.shaypolakicc.ui.playlists.VideoListViewModelFactory;
import icc.app.shaypolakicc.ui.video_player.VideoPlayerViewModelFactory;

public class InjectorUtils
{
    private InjectorUtils(){}

    public static PlayListsViewModelFactory providePlayListsViewModelFactory()
    {
        return new PlayListsViewModelFactory(PlayListsRepository.GetInstance());
    }

    public static VideoListViewModelFactory provideVideoListViewModelFactory()
    {
        return new VideoListViewModelFactory(VideoListRepository.GetInstance(null));
    }

    public static VideoPlayerViewModelFactory provideVideoPlayerViewModelFactory()
    {
        return new VideoPlayerViewModelFactory(VideoRepository.GetInstance(null));
    }
}
