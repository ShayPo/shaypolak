package icc.app.shaypolakicc.data.server;

import android.support.annotation.NonNull;

import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ApiClient
{
    //TODO: move this
    static private ApiClient mInstanse;
    private final String BASE_URL = "https://sandbox.cal-online.co.il/youtube/";
    private Retrofit mRetrofit;

    private ApiClient() {
        mRetrofit = new Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
//                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .baseUrl(BASE_URL)
                .build();
    }

    static synchronized public ApiClient GetInstance()
    {
        if(mInstanse == null)
        {
            mInstanse = new ApiClient();
        }
        return mInstanse;
    }

    public <T> T getAPI(@NonNull Class<T> apiClass)
    {
        return mRetrofit.create(apiClass);
    }
}
