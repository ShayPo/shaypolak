package icc.app.shaypolakicc.data;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;

import icc.app.shaypolakicc.data.model.VideoList;

public class VideoListRepository {
    static private VideoListRepository mInstanse;
    private MutableLiveData<VideoList> mVideoList;

    private VideoListRepository() {
        mVideoList = new MutableLiveData<>();
        mVideoList.setValue(null);
    }

    public LiveData<VideoList> getVideoList() {
        return mVideoList;
    }

    private void setVideoList(VideoList videoList) {
        mVideoList.setValue(videoList == null ? null : videoList);
    }

    static synchronized public VideoListRepository GetInstance(VideoList videolist) {
        if (mInstanse == null) {
            mInstanse = new VideoListRepository();
        }

        if (videolist != null) {
            mInstanse.setVideoList(videolist);
        }
        return mInstanse;
    }
}
