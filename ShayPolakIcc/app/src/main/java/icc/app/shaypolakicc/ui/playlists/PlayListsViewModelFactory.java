package icc.app.shaypolakicc.ui.playlists;

import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProvider;
import android.support.annotation.NonNull;

import icc.app.shaypolakicc.data.PlayListsRepository;

public class PlayListsViewModelFactory extends ViewModelProvider.NewInstanceFactory
{
    private PlayListsRepository mRepository;

    public PlayListsViewModelFactory(PlayListsRepository repository)
    {
        mRepository = repository;
    }

    @NonNull
    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        return (T) new PlayListsViewModel(mRepository);
    }
}
