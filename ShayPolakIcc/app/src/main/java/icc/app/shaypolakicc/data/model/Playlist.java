package icc.app.shaypolakicc.data.model;

import android.os.Parcel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Playlist
{
    @SerializedName("items")
    @Expose
    private List<Video> mVideos;

    public void setVideos(List<Video> playlistItems) {
        mVideos = playlistItems;
    }

    public List<Video> getVideos() {
        return mVideos;
    }
}
