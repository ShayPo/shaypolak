package icc.app.shaypolakicc.ui.playlists;

import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProvider;
import android.support.annotation.NonNull;

import icc.app.shaypolakicc.data.VideoListRepository;

public class VideoListViewModelFactory
        extends ViewModelProvider.NewInstanceFactory
{
    private VideoListRepository mRepository;

    public VideoListViewModelFactory(VideoListRepository repository)
    {
        mRepository = repository;
    }

    @NonNull
    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        return (T) new VideoListViewModel(mRepository);
    }
}
