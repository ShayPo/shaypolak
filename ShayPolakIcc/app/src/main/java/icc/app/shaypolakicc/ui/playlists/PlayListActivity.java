package icc.app.shaypolakicc.ui.playlists;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import icc.app.shaypolakicc.R;
import icc.app.shaypolakicc.data.VideoRepository;
import icc.app.shaypolakicc.data.model.Video;
import icc.app.shaypolakicc.data.model.VideoList;
import icc.app.shaypolakicc.ui.video_player.VideoPlayerActivity;
import icc.app.shaypolakicc.utilities.InjectorUtils;

public class PlayListActivity extends AppCompatActivity {

    private VideoListViewModel mViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video_lists);
        init();
    }

    private void init() {
        final PlayListAdapter adapter = new PlayListAdapter();
        RecyclerView list = findViewById(R.id.playlists_display);
        list.setLayoutManager(new GridLayoutManager(this, 2, LinearLayoutManager.VERTICAL, false));
        list.setAdapter(adapter);
        adapter.getObservable().observe(this, new Observer<View>() {
            @Override
            public void onChanged(@Nullable View v) {
                if(v != null) {
                    Intent showPlayList = new Intent(PlayListActivity.this, VideoPlayerActivity.class);
                    VideoRepository.GetInstance((Video) v.getTag());
                    startActivity(showPlayList);
                }
            }
        });

        mViewModel = ViewModelProviders.of(this, InjectorUtils.provideVideoListViewModelFactory()).get(VideoListViewModel.class);
        mViewModel.getVideoList().observe(this, new Observer<VideoList>() {
            @Override
            public void onChanged(@Nullable VideoList videoList) {
                if (videoList != null) {
                    adapter.updateData(videoList.getPlaylistItems().getVideos());
                }
            }
        });
    }

}
