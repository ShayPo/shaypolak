package icc.app.shaypolakicc.data;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.util.Log;

import icc.app.shaypolakicc.data.model.Playlist;
import icc.app.shaypolakicc.data.model.Playlists;
import icc.app.shaypolakicc.data.server.PlaylistsAPI;
import icc.app.shaypolakicc.data.server.ApiClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PlayListsRepository {
    static private PlayListsRepository mInstanse;
    private MutableLiveData<Playlists> mPlaylists;
    private PlaylistsAPI mAPI;

    private PlayListsRepository() {
        mPlaylists = new MutableLiveData<>();
        mPlaylists.setValue(new Playlists());
        mAPI = ApiClient.GetInstance().getAPI(PlaylistsAPI.class);

        Call<Playlists> call = mAPI.getPlaylists();
        call.enqueue(new Callback<Playlists>() {
            @Override
            public void onResponse(Call<Playlists> call, Response<Playlists> response) {
                if (response.isSuccessful()) {
                    mPlaylists.setValue(response.body());
                }
            }

            @Override
            public void onFailure(Call<Playlists> call, Throwable t) {
                Log.d("errorCall", t.getLocalizedMessage());
            }
        });
    }

    public LiveData<Playlists> getPlayLists() {
        return mPlaylists;
    }

    static synchronized public PlayListsRepository GetInstance() {
        if (mInstanse == null) {
            mInstanse = new PlayListsRepository();
        }
        return mInstanse;
    }
}
